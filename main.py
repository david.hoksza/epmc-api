#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Downloads all disease-related articles annotations from EPMC using their API. The output is
stored in a json file which contains information about the extracted named entities (genes and proteins)
and the disease-entity mapping.
"""

import sys
sys.path.append('..')

import requests
import common
import logging
import json
import argparse

__author__ = "David Hoksza"
__email__ = "david.hoksza@uni.lu"
__license__ = 'Apache 2.0'


def get_genes_for_disease(disease="parkinson's%20disease"):

    cm = 0
    dis_articles = []
    dis_rel_articles = []
    genes = []
    dis_rel_articles_ixs = {}

    while cm != -1:

        # if (len(genes) >0): break

        # Obtain publications with a PD annotation
        res = requests.get("https://www.ebi.ac.uk/europepmc/annotations_api/annotationsByEntity?entity={}&filter=1&format=ID_LIST&cursorMark={}&pageSize=8".format(disease, cm))
        if res.status_code == requests.codes.ok:
            content = json.loads(res.text)
            cm = content["nextCursorMark"]
            current_articles = content["articles"]
            # store all articles which mention the disease
            dis_articles += current_articles
            print("Articles count: ".format(len(dis_articles)))

            query_string = "&".join(["articleIds={}%3A{}".format(ca["source"], ca["extId"]) for ca in current_articles])
            # Obtain genes and proteins from papers mentioning the disease
            res_art_annot = requests.get("https://www.ebi.ac.uk/europepmc/annotations_api/annotationsByArticleIds?{}&type=Gene_Proteins&format=JSON".format(query_string))
            if res_art_annot.status_code == requests.codes.ok:
                art_annot_infos = json.loads(res_art_annot.text)
                # For every article, there is one dictionary with identifiers of the article and list of annotations, i.e. gene/protein mention in the text
                for art_annot_info in art_annot_infos:
                    for annot in art_annot_info["annotations"]:
                        # For every gene, let's check, whether it is in a relevant relation with the disease (the fact that it is mentioned in the paper
                        # which has the disease annotation does not have to mean that they are related in terms of, e.g., causality
                        # if (len(genes) > 0): break
                        gene = annot["exact"]
                        if gene not in genes:
                            print("Evaluating {}".format(gene))
                            genes.append(gene)
                            cm_relation = 0
                            while cm_relation != -1:

                                # Obtain articles which relate the obtained gene and the disease (this can, of course, include
                                # the article in which this gene was found (the outermost loop)
                                res_relation = requests.get("https://www.ebi.ac.uk/europepmc/annotations_api/annotationsByRelationship?firstEntity={}&secondEntity={}&filter=1&format=JSON&cursorMark={}&pageSize=8".format(
                                    gene, disease, cm_relation))
                                if res_relation.status_code == requests.codes.ok:
                                    res_relation_content = json.loads(res_relation.text)
                                    cm_relation = res_relation_content["nextCursorMark"]
                                    current_rel_articles = res_relation_content["articles"]
                                    for article in current_rel_articles:
                                        article_id = article["source"] + ":" + article["extId"]
                                        if article_id  in dis_rel_articles_ixs:
                                            dis_rel_articles[dis_rel_articles_ixs[article_id]]["annotations"] += article["annotations"]
                                        else:
                                            dis_rel_articles.append(article)
                                            dis_rel_articles_ixs[article_id] = len(dis_rel_articles) - 1
                                    # dis_rel_articles += current_rel_articles
                                    print("Relation articles count: {}".format(len(dis_rel_articles)))

    with common.open_file("{}.json".format(disease), "w") as f:
        f.write(json.dumps(dis_rel_articles, indent=4))


def main():
    # common.init_logging()
    get_genes_for_disease()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    args = parser.parse_args()

    main()